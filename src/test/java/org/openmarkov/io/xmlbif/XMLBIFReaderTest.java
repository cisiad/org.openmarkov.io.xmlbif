package org.openmarkov.io.xmlbif;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import java.io.File;
import java.net.URL;
import org.junit.Before;
import org.junit.Test;
import org.openmarkov.core.model.network.factory.BNFactory;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.ProbNetTest;
import org.openmarkov.io.xmlbif.XMLBIFReader;

public class XMLBIFReaderTest {
	   private String rootPath;
	    private XMLBIFReader reader;
	    
		private String networkTestName = "netAB.xml";

		@Before
	    public void setUp() throws Exception {
	    	URL url = getClass().getClassLoader ().getResource (networkTestName);
			File file = new File(url.getPath());
			String absolutePath = file.getAbsolutePath();
			rootPath = absolutePath.substring(0, absolutePath.length() - networkTestName.length());
			reader = new XMLBIFReader();
	    }
		
		@Test
		public void readNetworkTest() { 
			ProbNet probNet1;
			try {
				String pathAndName = rootPath + networkTestName;
				probNet1 = reader.loadProbNet(pathAndName).getProbNet ();
				assertNotNull(probNet1);
				ProbNetTest.compareNetworks(probNet1,BNFactory.createBN_XY("class","Symptom",0.34375,0.75,0.681818181818));
				
			} catch (Exception e) {
				fail("Exception: " + e.getLocalizedMessage());
			}  
		}
}
