package org.openmarkov.io.xmlbif.strings;

import java.io.Serializable;

public enum XMLBIFValues implements Serializable {
		NATURE("nature");
		
		private String name;

		XMLBIFValues(String name) {
			this.name = name;
		}
		
		public String toString() {
			return name;
		}

}
