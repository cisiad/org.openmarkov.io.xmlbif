package org.openmarkov.io.xmlbif.strings;

public enum XMLBIFTags {
	BIF,
	NETWORK,
	NAME,
	VARIABLE,
	OUTCOME,
	PROPERTY,
	DEFINITION,
	FOR,
	GIVEN,
	TABLE;

}
